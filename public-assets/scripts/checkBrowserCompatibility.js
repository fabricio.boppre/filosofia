// Function to replace special characters with their simplified correspondent:
function checkBrowserCompatibility(feature, platform) {
  const platformVersion = parseInt(
    platform.version.substring(0, platform.version.indexOf("."))
  );
  let compatible = true;
  switch (feature) {
    case "frameworkIntegration":
      if (
        (platform.name == "Safari" && platformVersion < 14) ||
        (platform.name == "Chrome" && platformVersion < 80) ||
        (platform.name == "Microsoft Edge" && platformVersion < 80) ||
        (platform.name == "Firefox" && platformVersion < 74) ||
        (platform.name == "Opera" && platformVersion < 67) ||
        platform.name == "IE"
      ) {
        compatible = false;
      }
      break;
  }
  return compatible;
}
