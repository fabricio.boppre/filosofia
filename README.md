# About

Read about this site [here](https://filosofia.fabricioboppre.net/sobre/) (in Portuguese).

# Stack

- [Astro](https://astro.build);
- [React](https://reactjs.org);
- [Lunr](https://lunrjs.com);
- [Nhost](http://nhost.io);
- [Hasura](https://hasura.io);
- [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

# Licenses

The source code of this site is shared under the [MIT license](LICENSE) and its content is under the [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) license.
