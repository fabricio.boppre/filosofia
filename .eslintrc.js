module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: ["eslint:recommended", "prettier"],
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
  },
  rules: {},
  overrides: [
    {
      files: ["*.astro"],
      parser: "astro-eslint-parser",
      rules: {},
    },
    {
      files: ["*.jsx"],
      extends: "plugin:react/recommended",
      rules: {
        "react/prop-types": "off",
      },
    },
  ],
};
