declare module 'astro:content' {
	interface Render {
		'.md': Promise<{
			Content: import('astro').MarkdownInstance<{}>['Content'];
			headings: import('astro').MarkdownHeading[];
			remarkPluginFrontmatter: Record<string, any>;
		}>;
	}
}

declare module 'astro:content' {
	export { z } from 'astro/zod';
	export type CollectionEntry<C extends keyof typeof entryMap> =
		(typeof entryMap)[C][keyof (typeof entryMap)[C]];

	// TODO: Remove this when having this fallback is no longer relevant. 2.3? 3.0? - erika, 2023-04-04
	/**
	 * @deprecated
	 * `astro:content` no longer provide `image()`.
	 *
	 * Please use it through `schema`, like such:
	 * ```ts
	 * import { defineCollection, z } from "astro:content";
	 *
	 * defineCollection({
	 *   schema: ({ image }) =>
	 *     z.object({
	 *       image: image(),
	 *     }),
	 * });
	 * ```
	 */
	export const image: never;

	// This needs to be in sync with ImageMetadata
	type ImageFunction = () => import('astro/zod').ZodObject<{
		src: import('astro/zod').ZodString;
		width: import('astro/zod').ZodNumber;
		height: import('astro/zod').ZodNumber;
		format: import('astro/zod').ZodUnion<
			[
				import('astro/zod').ZodLiteral<'png'>,
				import('astro/zod').ZodLiteral<'jpg'>,
				import('astro/zod').ZodLiteral<'jpeg'>,
				import('astro/zod').ZodLiteral<'tiff'>,
				import('astro/zod').ZodLiteral<'webp'>,
				import('astro/zod').ZodLiteral<'gif'>,
				import('astro/zod').ZodLiteral<'svg'>
			]
		>;
	}>;

	type BaseSchemaWithoutEffects =
		| import('astro/zod').AnyZodObject
		| import('astro/zod').ZodUnion<import('astro/zod').AnyZodObject[]>
		| import('astro/zod').ZodDiscriminatedUnion<string, import('astro/zod').AnyZodObject[]>
		| import('astro/zod').ZodIntersection<
				import('astro/zod').AnyZodObject,
				import('astro/zod').AnyZodObject
		  >;

	type BaseSchema =
		| BaseSchemaWithoutEffects
		| import('astro/zod').ZodEffects<BaseSchemaWithoutEffects>;

	type BaseCollectionConfig<S extends BaseSchema> = {
		schema?: S | (({ image }: { image: ImageFunction }) => S);
		slug?: (entry: {
			id: CollectionEntry<keyof typeof entryMap>['id'];
			defaultSlug: string;
			collection: string;
			body: string;
			data: import('astro/zod').infer<S>;
		}) => string | Promise<string>;
	};
	export function defineCollection<S extends BaseSchema>(
		input: BaseCollectionConfig<S>
	): BaseCollectionConfig<S>;

	type EntryMapKeys = keyof typeof entryMap;
	type AllValuesOf<T> = T extends any ? T[keyof T] : never;
	type ValidEntrySlug<C extends EntryMapKeys> = AllValuesOf<(typeof entryMap)[C]>['slug'];

	export function getEntryBySlug<
		C extends keyof typeof entryMap,
		E extends ValidEntrySlug<C> | (string & {})
	>(
		collection: C,
		// Note that this has to accept a regular string too, for SSR
		entrySlug: E
	): E extends ValidEntrySlug<C>
		? Promise<CollectionEntry<C>>
		: Promise<CollectionEntry<C> | undefined>;
	export function getCollection<C extends keyof typeof entryMap, E extends CollectionEntry<C>>(
		collection: C,
		filter?: (entry: CollectionEntry<C>) => entry is E
	): Promise<E[]>;
	export function getCollection<C extends keyof typeof entryMap>(
		collection: C,
		filter?: (entry: CollectionEntry<C>) => unknown
	): Promise<CollectionEntry<C>[]>;

	type ReturnTypeOrOriginal<T> = T extends (...args: any[]) => infer R ? R : T;
	type InferEntrySchema<C extends keyof typeof entryMap> = import('astro/zod').infer<
		ReturnTypeOrOriginal<Required<ContentConfig['collections'][C]>['schema']>
	>;

	const entryMap: {
		"concepts": {
"apeiron.md": {
  id: "apeiron.md",
  slug: "apeiron",
  body: string,
  collection: "concepts",
  data: any
} & { render(): Render[".md"] },
"arche.md": {
  id: "arche.md",
  slug: "arche",
  body: string,
  collection: "concepts",
  data: any
} & { render(): Render[".md"] },
"escola_eleata.md": {
  id: "escola_eleata.md",
  slug: "escola_eleata",
  body: string,
  collection: "concepts",
  data: any
} & { render(): Render[".md"] },
"escola_jonica.md": {
  id: "escola_jonica.md",
  slug: "escola_jonica",
  body: string,
  collection: "concepts",
  data: any
} & { render(): Render[".md"] },
"estoicismo.md": {
  id: "estoicismo.md",
  slug: "estoicismo",
  body: string,
  collection: "concepts",
  data: any
} & { render(): Render[".md"] },
"pre-socraticos.md": {
  id: "pre-socraticos.md",
  slug: "pre-socraticos",
  body: string,
  collection: "concepts",
  data: any
} & { render(): Render[".md"] },
},
"people": {
"anaximandro_de_mileto.md": {
  id: "anaximandro_de_mileto.md",
  slug: "anaximandro_de_mileto",
  body: string,
  collection: "people",
  data: any
} & { render(): Render[".md"] },
"anaximenes_de_mileto.md": {
  id: "anaximenes_de_mileto.md",
  slug: "anaximenes_de_mileto",
  body: string,
  collection: "people",
  data: any
} & { render(): Render[".md"] },
"empedocles_de_agrigento.md": {
  id: "empedocles_de_agrigento.md",
  slug: "empedocles_de_agrigento",
  body: string,
  collection: "people",
  data: any
} & { render(): Render[".md"] },
"heraclito_de_efeso.md": {
  id: "heraclito_de_efeso.md",
  slug: "heraclito_de_efeso",
  body: string,
  collection: "people",
  data: any
} & { render(): Render[".md"] },
"parmenides_de_eleia.md": {
  id: "parmenides_de_eleia.md",
  slug: "parmenides_de_eleia",
  body: string,
  collection: "people",
  data: any
} & { render(): Render[".md"] },
"pitagoras_de_samos.md": {
  id: "pitagoras_de_samos.md",
  slug: "pitagoras_de_samos",
  body: string,
  collection: "people",
  data: any
} & { render(): Render[".md"] },
"tales_de_mileto.md": {
  id: "tales_de_mileto.md",
  slug: "tales_de_mileto",
  body: string,
  collection: "people",
  data: any
} & { render(): Render[".md"] },
"xenofanes_de_colofao.md": {
  id: "xenofanes_de_colofao.md",
  slug: "xenofanes_de_colofao",
  body: string,
  collection: "people",
  data: any
} & { render(): Render[".md"] },
},

	};

	type ContentConfig = never;
}
