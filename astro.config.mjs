import { defineConfig } from "astro/config";
import sitemap from "@astrojs/sitemap";
import react from "@astrojs/react";

// https://astro.build/config
export default defineConfig({
  outDir: "./public",
  publicDir: "./public-assets",
  site: "https://filosofia.fabricioboppre.net",
  integrations: [react(), sitemap()],
});
