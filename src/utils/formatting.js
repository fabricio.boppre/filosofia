// Function to format the year of birth or death:
export function yearFormatting(year, estimation) {
  var yearFormatted;
  if (year < 0) {
    yearFormatted = Math.abs(year) + " a.C.";
  } else {
    yearFormatted = Math.abs(year);
  }
  if (estimation) {
    yearFormatted = "c. " + yearFormatted;
  }
  return yearFormatted;
}

// Function to get the slug from a Markdown filename:
export function slugFromFilename(filename) {
  return filename
    .substring(filename.length, filename.lastIndexOf("/") + 1)
    .replace(".md", "");
}
