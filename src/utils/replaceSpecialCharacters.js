// Function to replace special characters with their simplified correspondent:
export const replaceSpecialCharacters = (text) => {
  const replacements = {
    á: "a",
    ã: "a",
    â: "a",
    é: "e",
    ê: "e",
    í: "i",
    ó: "o",
    ô: "o",
    õ: "o",
    ç: "c",
  };
  let loweredCaseText = text.toLowerCase();
  return loweredCaseText.replace(/[áãâéêíóôõç]/g, (m) => replacements[m]);
};
