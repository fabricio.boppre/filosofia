Discípulo e continuador de [Anaximandro](/filosofo/anaximandro_de_mileto/), dedicou-se à meteorologia e foi o primeiro a formular a hipótese de que a lua recebe sua luz do sol. No que diz respeito à origem das coisas que compõe a natureza - a questão essencial da Escola Jônica -, Anaxímenes não associava-a à água, como Tales, nem ao [*Ápeiron*](/conceito/apeiron/) de seu mestre Anaximandro: para ele, o elemento essencial (o [*Arché*](/conceito/arche/)) era o ar.

Assim nos explica Simplício:

> Anaxímenes de Mileto, filho de Eurístrates, companheiro de Anaximandro, afirma também que uma só é a natureza subjacente, e diz, com aquele, que é ilimitada, não porém indefinida, como aquele diz, mas definida, dizendo que ela é o ar. Diferencia-se nas substâncias, por rarefação e condensação. Rarefazendo-se, torna-se fogo; condensando-se, vento, depois, nuvem, e ainda mais, água, depois terra, depois pedras, e as demais coisas provêm destas. Também ele faz eterno o movimento pelo qual se dá a transformação. [^1]

Hegel, em _Preleções sobre a História da Filosofia_, acrescenta:

> Ele [Anaxímenes] como que caracteriza a passagem da filosofia da natureza para a da consciência ou a renúncia ao modo objetivo do ser originário. [^2]

[^1]: Transcrito a partir do livro _Os Pensadores, Vol. I - Os Pré-socráticos_ (Editora Abril - 1ª edição, agosto de 1973), página 57. Tradução de Wilson Regis.
[^2]: Transcrito a partir do livro _Os Pensadores, Vol. I - Os Pré-socráticos_ (Editora Abril - 1ª edição, agosto de 1973), página 58. Tradução de Ernildo Stein.
