- Tido por muitos como o mais eminente pensador pré-socrático. Estabeleceu a existência de uma lei universal e fixa (o Lógos), regedora de todos os acontecimentos particulares e fundamento da harmonia universal.

- Diz Diógenes Laércio: "Eis, em linhas gerais, sua [Heráclito] doutrina: tudo se compõe a partir do fogo e nele se resolve; tudo se origina segundo o destino e por direções contrárias se harmonizam os seres; tudo está cheio de almas e demônios. (...) Tudo se origina por oposição e tudo flui como um rio (...)" (Os Pensadores Vol. I, pág. 82)

- Diz Platão: "Heráclito retira do universo a tranquilidade e a estabilidade, pois isso é próprio dos mortos, e atribuía movimento a todos os seres (...)" (Os Pensadores Vol. I, pág. 83)

- Fragmentos de Heráclito: "Tempo é criança brincando" (Os Pensadores Vol. I, pág. 90); "Para o Deus são belas todas as coisas e boas e justas, mas homens umas tomam como injustas, outras como justas." (Os Pensadores Vol. I, pág. 95)

- Diz Hegel: "A natureza é isto que jamais repousa, e o todo é a passagem de um para outro, da divisão para a unidade, da unidade para a divisão. (...) Nem um deus nem um homem fabricou o universo mas sempre foi e é e será um fogo sempre vivo, que segundo suas próprias leis (Métro) se acende e se apaga." Em outro trecho: "Os homens são deuses mortais e os deuses, homens imortais (...) O divino é o elevar-se, pelo pensamento, acima da pura natureza; essa faz parte da morte." (Os Pensadores Vol. I, pág. 102, 103 & 108)

- Diz Nietzsche: "Assim como ele [Heráclito] conheceu o tempo, conheceu-o, por exemplo, também Schopenhauer, ao enunciar repetidas vezes que nele cada instante só é na medida em que exterminou o instante anterior, seu pai, para também ser exterminado com a mesma rapidez; que passado e futuro são tão nulos quanto qualquer sonho, mas o presente é apenas o limite sem espessura e sem consistência entre ambos; e que, como o tempo, assim o espaço e, como este, assim também tudo o que está nele e no tempo só tem uma existência relativa, apenas por e para uma outra homogênea a ela, isto é, que também só subsiste do mesmo modo." (Os Pensadores Vol. I, pág. 109)
