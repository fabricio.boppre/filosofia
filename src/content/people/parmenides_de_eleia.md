- O filósofo grego Parmênides de Eléia foi o principal pensador da Escola Eleata. Suas teorias sucederam às ideias de Xenófanes e visavam a apresentar o imobilismo e a unidade como essência do surgimento do Universo. Parmênides fundou uma teoria que serviu de base para a filosofia platônica, e o seu embate com o pensamento de Heráclito forneceu bases para a filosofia desenvolvida pelos pensadores pluralistas. (https://brasilescola.uol.com.br/filosofia/parmenides.htm)

- Disse Aristóteles: "Uns negam absolutamente geração e corrupção, pois nenhum dos seres nasce ou perece, a não ser em aparência para nós. Tal é a doutrina da escola de Melisso e de Parmênides" (Os Pensadores Vol. I, pág. 145).

- Disse Sexto Empírico: "O movimento não existe segundo os filósofos da escola de Parmênides e Melisso. Aristóteles, num de seus diálogos relacionados à posição de Platão, os chama de imobilistas e não-físicos; imobilistas, porque são partidários da imobilidade; e não-físicos, porque a natureza é princípio de movimento, que eles negam, afirmando que nada se move." (Os Pensadores Vol. I, pág. 145).

- Trechos de sua obra "Sobre a natureza": "(...) que necessidade o teria impelido a depois ou antes, se do nada iniciado, nascer?"

- A teoria cosmológica de Parmênides difere-se muito das teorias apresentadas até então, aproximando-se apenas do princípio proposto por Xenófanes. Parmênides não formulou uma teoria cosmológica baseada em um princípio (arché) material e definido. Para o filósofo, havia uma espécie de organização racional no universo que era infinita, uma, indivisível, imutável e imóvel.

A teoria parmenidiana estava centrada no que ele chamou de “ser”. O ser das coisas era o princípio fundamental, baseado em uma espécie de ideia infinita e universal. Dessa maneira, tudo o que existia possuía o “ser” dentro de si. O que existe, ou seja, que possui o ser, pode ser enunciado e pensado. O que não existe não poderia, na visão do filósofo, ser pensado e nem enunciado. O problema legado à posteridade foi o problema do erro.

Para que o erro e a mentira existam, é necessária a existência do “não ser”. Como o “não ser” não é e não existe, como seria possível a existência do erro e da mentira? A resposta de Parmênides foi que o não ser, que permitia o erro e a mentira, era apenas uma ilusão causada pela opinião e pelos sentidos.

Para Parmênides, era dotado de existência somente aquilo que existe infinitamente e de maneira imóvel, ou seja, somente por meio das essências. A essência é o que aponta o ser existente em algo ou alguém. Essa essência é fixa, eterna e imutável, e as mudanças que percebemos nas coisas são, na realidade, fruto de nossos sentidos enganosos.

“O ser é e o não ser não é”, frase proferida por Parmênides, indica que o ser (o que existe) é, pois é idêntico a si mesmo e indica a si mesmo. O não ser não é, pois não existe, não possui identidade. As identidades são as definições de uma lógica rudimentar já utilizada por Parmênides, mas ainda muito próximas a uma metafísica.

A teoria do imobilismo universal, já iniciada por Xenófanes e aperfeiçoada por Parmênides, foi em grande medida utilizada pelo cristianismo para justificar a ideia de um Deus único, eterno e imutável. (Ver Os Pensadores Vol. I, pág. 175)

A grande motivação inicial da filosofia de Parmênides foi a contraposição às teses heraclitianas sobre o movimento e a mudança contínua de todas as coisas. Heráclito defendia que há um movimento contínuo (fluxo perpétuo) que permeia tudo o que existe, fazendo com que tudo mude a cada segundo. O princípio originário (arché) de todo o universo era o fogo, pois ele era o elemento que permitia a constante mudança e agitação. Parmênides defendeu teses completamente opostas, fixando as suas teorias no imobilismo e defendendo que a mudança era fruto das aparências. Os filósofos pré-socráticos que surgiram após Heráclito e Parmênides, classificados como pluralistas, tinham o propósito de resolver o problema deixado por esses dois pensadores. As vias pelas quais recorreram foram as de explicar a mudança e a essência das coisas por meio de mais de um elemento, que justificasse as diferenças, as mudanças, mas também a origem essencial de cada coisa.

A grande inspiração da filosofia platônica foi a teoria de Parmênides sobre o ser e o conceito das coisas. Ao defender que há uma essência infinita, eterna e imutável para tudo e que essa essência era, justamente, o ser, Parmênides ofereceu a Platão a ferramenta para a fundamentação do idealismo. Para Platão, o “ser” de Parmênides representava as ideias, eternas e imutáveis. A mudança era o fruto das aparências e dos enganos dos sentidos, que viam apenas as superfícies, que eram imperfeitas como toda a matéria.

(https://brasilescola.uol.com.br/filosofia/parmenides.htm)
