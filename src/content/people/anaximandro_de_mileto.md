Discípulo e sucessor de Tales, é considerado o primeiro a formular o conceito de uma lei universal presidindo o cosmos. Para explicar a origem do universo e de todos os seus elementos, elaborou o conceito de [*Ápeiron*](/conceito/apeiron/), algo indefinido e ilimitado que subjazeria a natureza de todas as coisas. Este seria, portanto, seu [*Arché*](/conceito/arche/), palavra grega que expressa a origem única de tudo que existe.

Teria escrito uma obra chamada _Sobre a natureza_, porém esta obra se perdeu, restando apenas referências feitas por filósofos posteriores.

Anaximandro fez também estudos de geografia e astronomia e ocupou cargos administrativos em Mileto.

Trecho de Simplício sobre Anaximandro de Mileto:

> Foi o primeiro a introduzir o termo _princípio_. Diz que este não é a água nem algum dos chamados elementos, mas alguma natureza diferente, ilimitada, e dela nascem os céus e os mundos neles contidos: 'Donde a geração... do tempo'. [^1]

Nietzsche, em _A Filosofia na Época Trágica dos Gregos_, chama Anaximandro de "o primeiro escritor filosófico":

> Enquanto o tipo universal do filósofo, na imagem de Tales, como que apenas se delineia de neblinas, já a imagem de seu grande sucessor nos fala muito mais claramente. Anaximandro de Mileto, o primeiro escritor filosófico dos antigos, escreve como escreverá o filósofo típico, enquanto solicitações alheias ainda não o despojaram de sua desenvoltura ou ingenuidade: em inscrições sobre pedra, estilo grandioso, frase pro frase, cada uma testemunha de uma nova iluminação e expressão do demorar-se em contemplações sublimes. O pensamento e sua forma são marcos de milha na senda que conduz àquela sabedoria altíssima. Nessa concisão lapidar, diz Anaximandro uma vez: 'De onde as coisas têm seu nascimento, ali também devem ir ao fundo, segundo a necessidade; pois têm de pagar penitência e de ser julgadas por suas injustiças, conforme a ordem do tempo'. [^2]

Outro trecho do mesmo livro de Nietzsche:

> Tales mostra a necessidade de simplificar o reino da pluralidade e reduzi-lo a um mero desdobramento ou disfarce da única qualidade existente, a água. Anaximandro o ultrapassa em dois passos. Pergunta-se, da primeira vez: 'Mas, se há em geral uma unidade eterna, como é possível aquela pluralidade?', e deduz a resposta do caráter contraditório dessa pluralidade, que consome e nega a si mesmo. Sua existência se toma para ele um fenômeno moral, que não se legitima, mas se penitencia, perpetuamente, pelo sucumbir. Mas, em seguida, ocorre-lhe a pergunta: 'Por que, então, tudo o que veio a ser já não foi ao fundo há muito tempo, uma vez que já transcorreu toda uma eternidade de tempo? De onde vem o fluxo sempre renovado do vir-a-ser?' Ele só sabe salvar-se dessa pergunta por possibilidades místicas: o vir-a-ser eterno só pode ter sua origem no ser eterno, as condições para o declínio daquele ser em um vir-a-ser na injustiça são sempre as mesmas, a constelação das coisas tem desde sempre uma índole tal que não se pode prever nenhum término para aquele sair dos seres isolados do seio do 'indeterminado'. Aqui ficou Anaximandro: isto é, ficou nas sombras profundas que, como gigantescos fantasmas, deitam-se sobre a montanha de uma tal contemplação do mundo. Quanto mais se procurava aproximar-se do problema - como, em geral, pode nascer, por declínio, do indeterminado o determinado, do eterno o temporal, do justo a injustiça -, maior se tornava a noite. [^3]

José Américo Motta Pessanha resume a filosofia de Anaximandro da seguinte forma:

> Para Anaximandro, o universo teria resultado de modificações ocorridas num princípio originário ou Arché. Esse princípio seria o Ápeiron, que se pode traduzir por infinito e/ou ilimitado. Desde a Antigüidade, discute-se se o Ápeiron pode ser interpretado como infinitude espacial, como indeterminação qualitativa, ou se envolve os dois aspectos. Certo é que, para Anaximandro, o Ápeiron estaria animado por um movimento eterno, que ocasionaria a separação dos pares de opostos. No único fragmento que restou de sua obra, Anaximandro afirma que, ao longo do tempo, os opostos pagam entre si as injustiças reciprocamente cometidas. Para alguns intérpretes isso significaria a afirmação da lei do equilíbrio universal, garantida através do processo de compensação dos excessos (por exemplo, no inverno, o frio seria compensado dos excessos cometidos pelo calor durante o verão). [^4]

[^1]: Transcrito a partir do livro _Os Pensadores, Vol. I - Os Pré-socráticos_ (Editora Abril - 1ª edição, agosto de 1973), página 21. Tradução de Wilson Regis.
[^2]: Transcrito a partir do livro _Os Pensadores, Vol. I - Os Pré-socráticos_ (Editora Abril - 1ª edição, agosto de 1973), página 23. Tradução de Rubens Rodrigues Torres Filho.
[^3]: Transcrito a partir do livro _Os Pensadores, Vol. I - Os Pré-socráticos_ (Editora Abril - 1ª edição, agosto de 1973), página 25. Tradução de Rubens Rodrigues Torres Filho.
[^4]: Transcrito a partir do livro _Os Pensadores - Os Pré-socráticos_ (Editora Abril - 2ª edição, 1978), página XXII.
