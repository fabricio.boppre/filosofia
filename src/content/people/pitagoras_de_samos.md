- Fundou uma doutrina de caráter mais religioso do que filosófico; acreditavam discernir a essência verdadeira das coisas em suas relações numéricas.

- Diz Nietzsche: "A contribuição original dos pitagóricos é (...) a significação do número e, portanto, a possibilidade de uma investigação exata em física. (...) Sua ideia fundamental é esta: a matéria, que é representada inteiramente destituída de qualidade, somente por relações numéricas adquire tal ou tal qualidade (...)." (Os Pensadores Vol. I, pág. 62-63).
