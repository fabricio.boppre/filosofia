import React, { useState } from "react";
import { yearFormatting } from "@utils/formatting";

export default function FilosofosIndex({ people }) {
  // Order states & change functions:
  const [philosophers, setPhilosophers] = useState(people);
  const [order, setOrder] = useState("date");
  const orderByName = () => {
    const ordered = philosophers.sort(function (a, b) {
      if (a.name > b.name) {
        return 1;
      }
      if (a.name < b.name) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
    setPhilosophers(ordered);
    setOrder("name");
  };
  const orderByDate = () => {
    const ordered = philosophers.sort(function (a, b) {
      if (a.birth_year > b.birth_year) {
        return 1;
      }
      if (a.birth_year < b.birth_year) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
    setPhilosophers(ordered);
    setOrder("date");
  };

  return (
    <>
      <div id="orderButtons">
        <span> ordernar por:</span>
        &nbsp;
        <button
          className={order == "date" ? "active" : undefined}
          onClick={orderByDate}
        >
          ano de nascimento
        </button>
        &nbsp;|&nbsp;
        <button
          className={order == "name" ? "active" : undefined}
          onClick={orderByName}
        >
          nome
        </button>
      </div>
      <ul>
        {philosophers ? (
          philosophers.map((philosopher) => (
            <li key={philosopher.slug}>
              <a href={`/filosofo/${philosopher.slug}`}>
                {philosopher.name}
                &nbsp;(
                {yearFormatting(
                  philosopher.birth_year,
                  philosopher.birth_year_estimation
                )}
                {philosopher.death_year
                  ? " - " +
                    yearFormatting(
                      philosopher.death_year,
                      philosopher.death_year_estimation
                    )
                  : ""}
                )
              </a>
            </li>
          ))
        ) : (
          <li>Não há ninguém por aqui neste momento.</li>
        )}
      </ul>
    </>
  );
}
