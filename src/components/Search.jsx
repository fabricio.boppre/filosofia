import React, { useState, useEffect, useRef } from "react";
import { replaceSpecialCharacters } from "@utils/replaceSpecialCharacters";
import lunr from "lunr";
import stemmerSupport from "lunr-languages/lunr.stemmer.support.js";
import pt from "lunr-languages/lunr.pt.js";

export default function Search({ documents }) {
  // State with the search results and ref object for search index and functions:
  const [results, setResults] = useState("");
  const idxRef = useRef();

  // Let's create a effect to, on the first render, create the Lunr search index and functions:
  // - We keep it on the ref object, so it persists for the full lifetime of the component;
  // - https://lunrjs.com
  // - https://reactjs.org/docs/hooks-reference.html#useref
  useEffect(() => {
    // We also start Lunr languages plugin:
    // - https://github.com/MihaiValentin/lunr-languages
    stemmerSupport(lunr);
    pt(lunr);
    // We want to search `text` and `title` fields, and the `reference` field will be our identifier (it brings the title and the path separated by `|`). Let’s define our index and add these documents to it:
    var idx = lunr(function () {
      this.use(lunr.pt);
      this.ref("reference");
      this.field("title", { boost: 10 });
      this.field("text");
      documents.forEach(function (doc) {
        this.add(doc);
      }, this);
    });
    idxRef.current = idx;
  }, []);

  // Perform the search when the form submit button is clicked:
  const runSearch = (event) => {
    // By using event.preventDefault(), we override the default browser behavior of redirecting the page:
    event.preventDefault();

    // Now we prepare our results and update its state, which will trigger a re-render of the page, showing then the results:
    var resultsResponse;
    if (
      idxRef.current.search(
        replaceSpecialCharacters(event.target.searchString.value)
      ).length > 0
    ) {
      resultsResponse = idxRef.current
        .search(replaceSpecialCharacters(event.target.searchString.value))
        .map((result) => {
          const textTitle = result.ref.substring(0, result.ref.indexOf("|"));
          const path = "/" + result.ref.substring(result.ref.indexOf("|") + 1);
          return (
            <li key={path}>
              <a href={path}>{textTitle}</a>
            </li>
          );
        });
      resultsResponse = <ul>{resultsResponse}</ul>;
    } else {
      resultsResponse = <p>Nada encontrado.</p>;
    }
    const searchResponse = (
      <div id="results">
        <h2>Resultado</h2>
        {resultsResponse}
      </div>
    );
    setResults(searchResponse);
  };

  return (
    <>
      <form onSubmit={runSearch}>
        <label hidden htmlFor="searchString">
          Pesquisar:
        </label>
        <input id="searchString" name="searchString" type="text" required />
        <button type="submit">
          <img
            src="/images/search-button.svg"
            alt="Pesquisar"
            width="20"
            height="20"
          />
        </button>
      </form>
      {results}
    </>
  );
}
